import time

import requests
import os
import logging

from telegram import Bot
from log_handler import LogsHandler


AUTH_TOKEN = os.getenv("DEVMAN_AUTH_TOKEN")
AUTH_HEADER = {"Authorization": f"Token {AUTH_TOKEN}"}
BOT_TOKEN = os.getenv("TG_BOT_TOKEN")
CHAT_ID = os.getenv("CHAT_ID")
TIMEOUT = 200
SLEEP_TIMEOUT = 5 * 60


def compose_message(data: dict) -> str:
    message = f"Работа по уроку {data['lesson_title']} (сдана {data['submitted_at'][:10]}) проверена. "

    if data["is_negative"]:
        message = message + "Обнаружены ошибки, проверьте примечания проверяющего на сайте. "
    else:
        message = message + "Ошибки не обнаружены. Можете приступать к следующему уроку модуля. "

    message = message + f"Подробности по ссылке: {data['lesson_url']}"

    return message


def get_devman_results_by_long_polling(send_message: callable) -> None:
    url = "https://dvmn.org/api/long_polling/"
    timestamp = None

    while True:
        try:
            response = requests.get(
                url,
                params={'timestamp': timestamp},
                timeout=TIMEOUT,
                headers=AUTH_HEADER,
            )
            response.raise_for_status()

            response_data = response.json()
            status = response_data["status"]

            if status == "timeout":
                timestamp = response_data['timestamp_to_request']

            elif status == "found":
                timestamp = response_data['last_attempt_timestamp']

                for attempt_data in response_data["new_attempts"]:
                    message = compose_message(attempt_data)
                    send_message(message)

        except requests.Timeout:
            continue

        except requests.ConnectionError:
            logging.warning("Failed to connect. Retry later...")
            time.sleep(SLEEP_TIMEOUT)


if __name__ == '__main__':
    bot = Bot(BOT_TOKEN)

    def send_bot_message(message: str) -> None:
        bot.send_message(text=message, chat_id=CHAT_ID)

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.addHandler(LogsHandler(send_bot_message))
    logging.info("Bot-backend is on and running...")

    get_devman_results_by_long_polling(send_bot_message)
