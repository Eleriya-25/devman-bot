import logging


class LogsHandler(logging.Handler):
    def __init__(self, emit_log_entry):
        super().__init__()
        self.emit_log_entry = emit_log_entry

    def emit(self, record):
        log_entry = self.format(record)
        self.emit_log_entry(log_entry)
