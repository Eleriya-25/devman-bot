# DEVMAN BOT

Сервис для получения обновления о проверках работ на платформе DEVMAN.
(см. main.get_devman_results_by_long_polling).


## Перед запуском

1. Установка poetry - `pip3 install poetry`. Poetry - менеджер зависимостей. Все зависимости перечислены в файле pyproject.toml.
2. Задайте переменные окружения:
`AUTH_TOKEN` - токен авторизации для API DEVMAN (см. https://dvmn.org/api/docs/),
`BOT_TOKEN` - токен телеграм-чат-бота,
`CHAT_ID` - идентификатор телеграм-чата, в который пишет бот.


## Запуск

`poetry run python ./main.py`

После запуска вы должны получить сообщение от бота о начале работы.


## Развертка

Код запущен на сервере Heroku. Приложение - devman-bot-bla-bla.